// Copyright Alexander Demets

#include "RuiLayoutContainer.h"

// Sets default values
ARuiLayoutContainer::ARuiLayoutContainer()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ARuiLayoutContainer::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ARuiLayoutContainer::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

