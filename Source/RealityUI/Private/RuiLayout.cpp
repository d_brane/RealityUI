// Copyright Alexander Demets

#include "RuiLayout.h"

// Sets default values
ARuiLayout::ARuiLayout()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ARuiLayout::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ARuiLayout::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

