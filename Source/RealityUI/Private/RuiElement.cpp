// Copyright Alexander Demets

#include "RuiElement.h"

// Sets default values
ARuiElement::ARuiElement()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ARuiElement::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ARuiElement::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

