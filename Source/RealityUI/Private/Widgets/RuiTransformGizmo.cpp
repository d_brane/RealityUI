// Copyright Alexander Demets

#include "Widgets/RuiTransformGizmo.h"

// Sets default values
ARuiTransformGizmo::ARuiTransformGizmo()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	GizmoMode = ETransformGizmoType::UniversalGizmo;

}

// Called when the game starts or when spawned
void ARuiTransformGizmo::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ARuiTransformGizmo::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

