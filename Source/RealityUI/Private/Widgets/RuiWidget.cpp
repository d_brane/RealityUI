// Copyright Alexander Demets

#include "Widgets/RuiWidget.h"

// Sets default values
ARuiWidget::ARuiWidget()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ARuiWidget::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ARuiWidget::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

