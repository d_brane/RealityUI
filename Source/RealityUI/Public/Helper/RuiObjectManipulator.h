// Copyright Alexander Demets

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
#include "RuiObjectManipulator.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class REALITYUI_API URuiObjectManipulator : public USceneComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	URuiObjectManipulator();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
