// Copyright Alexander Demets

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "RuiTransformGizmo.generated.h"


#define ECC_SelectTraceChannel ECC_GameTraceChannel8

//UENUM(BlueprintType)
//enum class ETransformWidgetSelectionMode : uint8
//{
//	Actor		UMETA(DisplayName = "Actor"),
//	Component	UMETA(DisplayName = "Component")
//};

UENUM(BlueprintType)
enum class ETransformWidgetReselectAction : uint8
{
	Nothing				UMETA(DisplayName = "Nothing"),
	Unselect			UMETA(DisplayName = "Unselect"),
	CycleTransformType	UMETA(DisplayName = "Cycle Transform Type")
};


UENUM(BlueprintType)
enum class ETransformGizmoType : uint8
{
	UniversalGizmo	UMETA(DisplayName = "Universal Gizmo"),
	Translation		UMETA(DisplayName = "Translation Gizmo"),
	Rotation		UMETA(DisplayName = "Rotation Gizmo"),
	Scale			UMETA(DisplayName = "Scale Gizmo"),
	Joint			UMETA(DisplayName = "Joint Gizmo")
};

UENUM(BlueprintType)
enum class ETransformManipulationMode : uint8
{
	FullTransform	UMETA(DisplayName = "Full Transform"),
	Position		UMETA(DisplayName = "Position"),
	Rotation		UMETA(DisplayName = "Rotation"),
	Scale			UMETA(DisplayName = "Scale"),
	NUM             UMETA(Hidden)
};

UENUM(BlueprintType)
enum class ETransformManipulationLockAxis : uint8
{
	None	UMETA(DisplayName = "Lock to None"),
	X		UMETA(DisplayName = "Lock to X"),
	Y		UMETA(DisplayName = "Lock to Y"),
	Z		UMETA(DisplayName = "Lock to Z"),
	XY		UMETA(DisplayName = "Lock to XY"),
	XZ		UMETA(DisplayName = "Lock to XZ"),
	YZ		UMETA(DisplayName = "Lock to YZ"),
};

UCLASS()
class REALITYUI_API ARuiTransformGizmo : public AActor
{
	GENERATED_BODY()
	
protected:

	TWeakObjectPtr<USceneComponent> BoundComponent;

	UPROPERTY(BlueprintReadWrite)
	ETransformGizmoType GizmoMode;
	
public:	

	// Sets default values for this actor's properties
	ARuiTransformGizmo();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//UFUNCTION(BlueprintCallable, Category = "DancingMeshes|Manipulator")
	//virtual void Setup(const USceneComponent * BindToComponent, )

	UFUNCTION(BlueprintCallable, Category = "DancingMeshes|Manipulator")
	virtual void BindTo(USceneComponent * BindToComponent) {
		BoundComponent = BindToComponent;
	}

	UFUNCTION(BlueprintCallable, Category = "DancingMeshes|Manipulator")
	virtual void SetBounds(FVector Origin, FVector BoxExtends) {
		;
	}

	UFUNCTION(BlueprintCallable, Category = "DancingMeshes|Manipulator")
	virtual void BoundsFrom(FVector Origin, FVector BoxExtends) {
		;
	}

	UFUNCTION(BlueprintCallable, Category = "DancingMeshes|Manipulator")
	virtual USceneComponent * GetBoundComponent() const {
		return BoundComponent.Get();
	}
};
