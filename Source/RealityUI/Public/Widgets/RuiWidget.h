// Copyright Alexander Demets

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "RuiWidget.generated.h"

UCLASS()
class REALITYUI_API ARuiWidget : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ARuiWidget();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
