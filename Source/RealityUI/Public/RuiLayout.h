// Copyright Alexander Demets

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "RuiLayout.generated.h"

UCLASS()
class REALITYUI_API ARuiLayout : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ARuiLayout();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
