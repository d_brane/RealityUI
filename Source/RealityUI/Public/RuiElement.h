// Copyright Alexander Demets

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "RuiElement.generated.h"

UCLASS()
class REALITYUI_API ARuiElement : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ARuiElement();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
