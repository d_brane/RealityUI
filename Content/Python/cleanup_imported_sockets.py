# script to remove trailing _(##) from imported sockets in StaticMeshes
# @d_brane get_editor_property("foo") should be all you need iirc?
# Basically in C++ there's two flags for a UPROPERTY, "BlueprintReadWrite" and "EditAnywhere"
# Things with "BlueprintReadWrite" get registered as a full property (ie: relative_location in the unreal.StaticMeshSocket)
# So you can just write "mySocket.relative_location = unreal.Vector(1,2,3)"

# d_brane [7:27 PM]
# huh, that's cool, so I might actually get lucky browsing through the header file from UStaticMesh, cool!

# lordned [7:28 PM]
# But for things that only have EditAnywhere, they have to be accessed as an editor property, so you have to write "mySocket.set_editor_property("preview_static_mesh", unreal.StaticMesh(...))"
# Just use the python `help` command and it'll tell you which functions/variables are exposed and hwo to access them

import unreal as ue

# not working :(
def get_assets_by_folder(directory):
    #print ("Looking for assets in folder: " + directory)
    # asset_registry = ue.AssetRegistryHelpers.get_asset_registry()
    # assets = asset_registry.get_assets_by_path(directory)
    # return assets
    # print(ue.EditorAssetLibrary.does_directory_have_assets(directory))
    return 0


def get_assets_by_selection():
    @ue.uclass()
    class MyEditorUtility(ue.GlobalEditorUtilityBase):
        pass

    editor_util = MyEditorUtility()
    selected_assets =  editor_util.get_selected_assets()
    return selected_assets

print ("executing cleanup sockets script...")

# rui_geo_dir    = ue.SystemLibrary.get_project_directory() + "plugins/RealityUI/Content/Geometry"
# rui_geo_assets = get_assets_by_folder(rui_geo_dir)
assets = get_assets_by_selection()


print ("Access assets: ")
for asset in assets:
    print (asset)
    # print (asset.get_editor_property())
    # help(asset)



print ("finished cleanup sockets script...")


